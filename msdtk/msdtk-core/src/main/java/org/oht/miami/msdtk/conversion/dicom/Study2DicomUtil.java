/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.conversion.dicom;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;

import java.io.FileNotFoundException;
import java.util.Iterator;

/**
 * Utilities for the conversion from study model to DICOM object.
 * 
 * @author Mahmoud Ismail(maismail@cs.jhu.edu)
 */
public class Study2DicomUtil {
    /**
     * Helper method for copying attributes between <code>DicomObject</code>.
     * 
     * @param source
     *            the object containing attributes to copy
     * @param destination
     *            the object to copy attributes to
     * @throws FileNotFoundException
     */
    public static void copyDicomObject(final DicomObject source, final DicomObject destination)
            throws FileNotFoundException {
        if (source == null) {
            return;
        }
        for (Iterator<DicomElement> iter = source.iterator(0x00020000, 0xffffffff); iter.hasNext();) {
            destination.add(iter.next());
        }
    }

}
