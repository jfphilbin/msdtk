/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
package org.oht.miami.msdtk.util;

import java.io.File;
import java.io.IOException;

/**
 * Set of utilities for file manipulation
 * 
 * @author rstonebr
 */
public class FileUtils {
    /**
     * Recursive deletes a directory or file
     * 
     * @param path
     *            to be deleted
     * @throws IOException
     */
    public static void deleteFile(File path) throws IOException {
        if (path.isDirectory()) {
            for (File child : path.listFiles()) {
                deleteFile(child);
            }
        }
        if (!path.delete()) {
            throw new IOException("Could not delete " + path);
        }
    }

}
