/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.util;

/*
 * This class complement the DicomElement interface. 
 * It has a set of functions to handle DicomElement
 * It can be added to the DicomElement interface, however we 
 * preferred not to change the implementation of the dcm4che2 library
 * */
import org.oht.miami.msdtk.studymodel.BulkDataDicomElement;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.VR;

import java.util.Iterator;

/**
 * Utilities class to process dcm4che2.0 Dicom elements.
 */
public class DicomElementUtil {

    /**
     * Compares the contents of 2 dicomElements, return true if equal
     * 
     * @param attribute1
     *            First dicomElement.
     * @param attribute2
     *            Second dicomElement
     * @param compareBulkDataReferences
     *            Whether or not to compare attributes with VR of BD. If true,
     *            the bulk data value lengths and the original VRs will be
     *            compared. If false, BD attributes are always considered
     *            different.
     * @return boolean determines if the inputs are equal or not.
     */
    public static boolean compareDicomElements(final DicomElement attribute1,
            final DicomElement attribute2, boolean compareBulkDataReferences) {

        if (attribute1.vr() != attribute2.vr()) {
            return false;
        }
        if (attribute1.countItems() != attribute2.countItems()) {
            return false;
        }
        // If both are simple dicom elements
        if (!attribute1.hasDicomObjects()) {
            if (attribute1.vr() == VR.BD) {
                if (!compareBulkDataReferences) {
                    return false;
                }
                BulkDataDicomElement bulkDataAttribute1 = (BulkDataDicomElement) attribute1;
                BulkDataDicomElement bulkDataAttribute2 = (BulkDataDicomElement) attribute2;
                return bulkDataAttribute1.getBulkDataValueLength() == bulkDataAttribute2
                        .getBulkDataValueLength()
                        && bulkDataAttribute1.getBulkDataVR() == bulkDataAttribute2.getBulkDataVR();
            } else {
                return java.util.Arrays.equals(attribute1.getBytes(), attribute2.getBytes());
            }
        }
        for (int i = 0; i < attribute1.countItems(); i++) {
            DicomObject item1 = attribute1.getDicomObject(i);
            DicomObject item2 = attribute2.getDicomObject(i);
            if (item1.size() != item2.size()) {
                return false;
            }
            Iterator<DicomElement> item1Iter = item1.iterator();
            Iterator<DicomElement> item2Iter = item2.iterator();
            while (item1Iter.hasNext()) {
                if (!compareDicomElements(item1Iter.next(), item2Iter.next(),
                        compareBulkDataReferences)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static int getCount(DicomElement dcmElem) {
        int count = 1;
        if (dcmElem.vr() == VR.SQ) {
            count = 0;
            for (int i = 0; i < dcmElem.countItems(); i++) {
                DicomObject childObj = dcmElem.getDicomObject(i);
                /* Add Attributes */
                for (Iterator<DicomElement> iter = childObj.iterator(0x00020000, 0xffffffff); iter
                        .hasNext();) {
                    count += getCount(iter.next());
                }
            }
        }
        return count;
    }
}
