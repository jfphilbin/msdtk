/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.studymodel;

import org.dcm4che2.data.Tag;

import java.util.HashMap;
import java.util.Map;

/**
 * Class holds the list of attributes defined to be study tags. Reference:
 * PS(3.3), tables C.7-1, C.7-3 & C.7-4.
 * 
 * @author maismail
 */
public abstract class StudyTags {
    /* Map of all possible study tags and the associated description */
    final static Map<Integer, String> map = new HashMap<Integer, String>(64);
    /* Flag to ensure the list of study tags are inserted in Map */
    static boolean isInitialized = false;

    private static void initialize() {
        isInitialized = true;
        // Add all possible study Tags
        // TODO(maismail) Update the list of attributes according to the tables
        // lised above.
        map.put(Tag.StudyDate, "Study Date");
        map.put(Tag.StudyTime, "Study Time");
        map.put(Tag.AccessionNumber, "Accession Number");
        map.put(Tag.IssuerOfAccessionNumberSequence, "Issuer Of Accession Number Sequence");
        map.put(Tag.ReferringPhysicianName, "Referring Physician Name");
        map.put(Tag.ReferringPhysicianIdentificationSequence,
                "Referring Physician Identification Sequence");
        map.put(Tag.StudyDescription, "Study Description");
        map.put(Tag.ProcedureCodeSequence, "Procedure Code Sequence");
        map.put(Tag.PhysiciansOfRecord, "Physicians Of Record");
        map.put(Tag.PhysiciansOfRecordIdentificationSequence,
                "Physicians Of Record Identification Sequence");
        map.put(Tag.NameOfPhysiciansReadingStudy, "Name Of Physicians Reading Study");
        map.put(Tag.PhysiciansReadingStudyIdentificationSequence,
                "Physicians Reading Study Identification Sequence");
        map.put(Tag.AdmittingDiagnosesDescription, "Admitting Diagnoses Description");
        map.put(Tag.AdmittingDiagnosesCodeSequence, "Admitting Diagnoses Code Sequence");
        map.put(Tag.ReferencedStudySequence, "Referenced Study Sequence");
        map.put(Tag.ReferencedPatientSequence, "Referenced Patient Sequence");
        map.put(Tag.PatientName, "Patient Name");
        map.put(Tag.PatientID, "Patient ID");
        map.put(Tag.PatientBirthDate, "Patient Birth Date");
        map.put(Tag.PatientBirthTime, "Patient Birth Time");
        map.put(Tag.PatientSex, "Patient Sex");
        map.put(Tag.OtherPatientIDs, "Other Patient ID");
        map.put(Tag.OtherPatientNames, "Other Patient Names");
        map.put(Tag.OtherPatientIDsSequence, "Other Patient IDs Sequence");
        map.put(Tag.PatientAge, "Patient Age");
        map.put(Tag.PatientSize, "Patient Size");
        map.put(Tag.PatientWeight, "Patient Weight");
        map.put(Tag.PatientSizeCodeSequence, "Patient's Size Code Sequence");
        map.put(Tag.EthnicGroup, "Ethnic Group");
        map.put(Tag.Occupation, "Occupation");
        map.put(Tag.AdditionalPatientHistory, "Additional Patient History");
        map.put(Tag.PatientSpeciesDescription, "Patient Species Description");
        map.put(Tag.PatientSpeciesCodeSequence, "Patient Species Code Sequence");
        map.put(Tag.PatientSexNeutered, "Patient Sex Neutered");
        map.put(Tag.PatientBreedDescription, "Patient Breed Description");
        map.put(Tag.PatientBreedCodeSequence, "Patient Breed Code Sequence");
        map.put(Tag.BreedRegistrationSequence, "Breed Registration Sequence");
        map.put(Tag.ResponsiblePerson, "Responsible Person");
        map.put(Tag.ResponsiblePersonRole, "Responsible Person Role");
        map.put(Tag.ResponsibleOrganization, "Responsible Organization");
        map.put(Tag.PatientComments, "Patient Comments");
        map.put(Tag.ClinicalTrialSponsorName, "Clinical Trial Sponsor Name");
        map.put(Tag.ClinicalTrialProtocolID, "Clinical Trial Protocol ID");
        map.put(Tag.ClinicalTrialProtocolName, "Clinical Trial Protocol Name");
        map.put(Tag.ClinicalTrialSiteID, "Clinical Trial Site ID");
        map.put(Tag.ClinicalTrialSiteName, "Clinical Trial Site Name");
        map.put(Tag.ClinicalTrialSubjectID, "Clinical Trial Subject ID");
        map.put(Tag.ClinicalTrialSubjectReadingID, "Clinical Trial Subject Reading ID");
        map.put(Tag.ClinicalTrialTimePointID, "Clinical Trial Time Point ID");
        map.put(Tag.ClinicalTrialTimePointDescription, "Clinical Trial Time Point Description");
        map.put(Tag.PatientIdentityRemoved, "Patient Identity Removed");
        map.put(Tag.DeidentificationMethod, "Deidentification Method");
        map.put(Tag.DeidentificationMethodCodeSequence, "Deidentification Method Code Sequence");
        map.put(Tag.ClinicalTrialProtocolEthicsCommitteeName,
                "Clinical Trial Protocol Ethics Committee Name");
        map.put(Tag.ClinicalTrialProtocolEthicsCommitteeApprovalNumber,
                "Clinical Trial Protocol Ethics Committee Approval Number");
        map.put(Tag.ConsentForClinicalTrialUseSequence, "Consent For Clinical Trial Use Sequence");
        map.put(Tag.DistributionType, "Distribution Type");
        map.put(Tag.ConsentForDistributionFlag, "Consent For Distribution Flag");
        map.put(Tag.StudyInstanceUID, "Study Instance UID");
        map.put(Tag.StudyID, "Study ID");
        map.put(Tag.RequestingServiceCodeSequence, "Requesting Service Code Sequence");
        map.put(Tag.AdmissionID, "Admission ID");
        map.put(Tag.IssuerOfAdmissionIDSequence, "Issuer Of Admission ID Sequence");
        map.put(Tag.ServiceEpisodeID, "Service Episode ID");
        map.put(Tag.ServiceEpisodeDescription, "Service Episode Description");
        map.put(Tag.IssuerOfServiceEpisodeIDSequence, "Issuer Of Service Episode ID Sequence");
        map.put(Tag.ReasonForPerformedProcedureCodeSequence,
                "Reason For Performed Procedure Code Sequence");
    }

    /* Function to check if a certain tag is a study tag or not */
    public static boolean isStudyTag(int tag) {
        if (!isInitialized) {
            initialize();
        }
        return map.containsKey(tag);
    }
}