/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
package org.oht.miami.msdtk.store;

import java.io.IOException;

/**
 * An exception that denotes that a version has already been deleted and thus
 * cannot be rewritten to the store.
 * 
 * @author Ryan R Stonebraker rstonebr@harris.com
 */
public class VersionPreviouslyDeletedException extends IOException {
    static final long serialVersionUID = 1L;

    public VersionPreviouslyDeletedException() {
        super("VersionPreviouslyDeletedException");
    }

    public VersionPreviouslyDeletedException(String s) {
        super(s);
    }

}
