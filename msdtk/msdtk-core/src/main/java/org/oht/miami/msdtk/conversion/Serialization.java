/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.conversion;

import org.oht.miami.msdtk.studymodel.Study;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

/**
 * Class to serialize a study object to a byte array and vice versa.
 * 
 * @author mmahmou4
 */
public class Serialization {

    /**
     * Convert a serialized byte array to a study object.
     * 
     * @param serialization
     * @return Study
     */
    public static Study serializationToStudy(final byte[] serialization) throws IOException,
            ClassNotFoundException {
        ObjectInput inObj = new ObjectInputStream(new ByteArrayInputStream(serialization));
        Study study = null;
        try {
            study = (Study) inObj.readObject();
        } finally {
            inObj.close();
        }
        return study;
    }

    /**
     * Serialize the study object and convert it to a byte array
     * 
     * @param study
     *            Study
     * @return byte array
     */
    public static byte[] serializeMetaData(final Study study) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput outObj = new ObjectOutputStream(bos);
        try {
            outObj.writeObject(study);
            return bos.toByteArray();
        } finally {
            outObj.close();
        }

    }
}
