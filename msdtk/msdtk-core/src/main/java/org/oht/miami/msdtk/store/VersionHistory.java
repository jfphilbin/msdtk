/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.store;

import org.oht.miami.msdtk.studymodel.Study;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.SequenceDicomElement;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * Version history object storing set of VersionInfo objects
 * 
 * @author Ryan Stonebraker (rstonebr@harris.com)
 */
public class VersionHistory implements Serializable, Iterable<VersionInfo> {

    private static final long serialVersionUID = 8791145563280098349L;
    /**
     * List of version info history objects for the study
     */
    private List<VersionInfo> versionInfos = null;

    /**
     * Constructor
     */
    public VersionHistory() {
        versionInfos = new ArrayList<VersionInfo>();
    }

    /**
     * Add version to history
     * 
     * @param version
     *            version to be added
     * @param uuid
     *            storage uuid of the version
     */
    public void addVersion(Study version, UUID uuid) {
        VersionInfo versionInfo = new VersionInfo(uuid, getMostRecentVersionNumber() + 1,
                version.getVersionType(), new Date());
        versionInfos.add(versionInfo);
    }

    /**
     * Get most recent version number in history
     * 
     * @return latest version number
     */
    public int getMostRecentVersionNumber() {
        return versionInfos.size() - 1;
    }

    /**
     * Get most recent version info in history
     * 
     * @return latest version info
     */
    public VersionInfo getMostRecentVersionInfo() {
        if (versionInfos.isEmpty()) {
            return null;
        }
        return versionInfos.get(getMostRecentVersionNumber());
    }

    /**
     * Get version info
     * 
     * @param versionNumber
     *            of version to be returned
     * @return version info
     */
    public VersionInfo getVersionInfo(int versionNumber) {
        return versionInfos.get(versionNumber);
    }

    /**
     * Encodes the entire version history into a Sequence.
     * 
     * @return The encoded Sequence.
     */
    public DicomElement encodeVersionHistory() {
        DicomElement prevStudySeq = new SequenceDicomElement(Tag.PreviousStudyVersionsSequence,
                VR.SQ, false, new ArrayList<Object>(versionInfos.size()), null);
        for (int i = versionInfos.size() - 1; i >= 0; --i) {
            VersionInfo info = versionInfos.get(i);
            DicomObject previousVersionItem = new BasicDicomObject();
            previousVersionItem.putString(Tag.StudyVersionUUID, VR.LO, info.getUUID().toString());
            previousVersionItem.putInt(Tag.StudyVersionNumber, VR.US, info.getVersionNumber());
            previousVersionItem.putString(Tag.StudyVersionType, VR.CS, info.getType().toString());
            // TODO putDate() doesn't encode time zone
            // information
            previousVersionItem.putDate(Tag.StudyVersionCreationDateTime, VR.DT,
                    info.getCreationDate());
            previousVersionItem.putString(Tag.StudyVersionCompressionType, VR.LO,
                    info.getCompressionType());
            prevStudySeq.addDicomObject(previousVersionItem);
        }
        return prevStudySeq;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((versionInfos == null) ? 0 : versionInfos.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        VersionHistory other = (VersionHistory) obj;
        if (versionInfos == null) {
            if (other.versionInfos != null)
                return false;
        } else if (!versionInfos.equals(other.versionInfos))
            return false;
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (VersionInfo versionInfo : versionInfos) {
            sb.append(versionInfo.toString() + " \n");
        }
        return sb.toString();
    }

    public boolean contains(VersionHistory other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (versionInfos == null) {
            if (other.versionInfos != null) {
                return false;
            }
        } else if (!versionInfos.containsAll(other.versionInfos)) {
            return false;
        }
        return true;
    }

    @Override
    public Iterator<VersionInfo> iterator() {
        return versionInfos.iterator();
    }

}
