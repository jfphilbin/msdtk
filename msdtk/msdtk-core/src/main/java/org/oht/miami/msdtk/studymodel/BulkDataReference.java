/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.studymodel;

import org.oht.miami.msdtk.store.BulkDataSet;

import org.dcm4che2.data.VR;
import org.dcm4che2.util.ByteUtils;

import java.io.IOException;
import java.io.Serializable;

/**
 * A BulkDataReference (BDR) is a reference to the actual bulk data value in the
 * study's bulk data storage.
 * 
 * @author Raphael Yu Ning
 */
public class BulkDataReference implements Serializable {

    private static final long serialVersionUID = 7315808593188490484L;

    public static final int BDR_VALUE_LENGTH = 16;

    private VR vr;

    private BulkDataSet bulkData;

    private int index;

    private int offset;

    private int length;

    private boolean isEndianToggled;

    public BulkDataReference(VR vr, BulkDataSet bulkData, int index, int offset, int length) {

        if (bulkData == null) {
            throw new IllegalArgumentException("BDR must refer to a non-null BulkData object");
        }
        if (index < 0 || index >= bulkData.getNumberOfItems()) {
            throw new IndexOutOfBoundsException("Invalid BDR Index " + index);
        }
        if (offset < 0) {
            throw new IllegalArgumentException("BDR Offset must not be negative");
        }
        if (length <= 0) {
            throw new IllegalArgumentException("BDR Length must be positive");
        }

        this.vr = vr;
        this.bulkData = bulkData;
        this.index = index;
        this.offset = offset;
        this.length = length;
        isEndianToggled = false;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        } else if (!(o instanceof BulkDataReference)) {
            return false;
        } else {
            BulkDataReference other = (BulkDataReference) o;
            return vr.equals(other.vr) && bulkData == other.bulkData && index == other.index
                    && offset == other.offset && length == other.length;
        }
    }

    @Override
    public int hashCode() {

        return offset + (length + index * 31) * 31;
    }

    @Override
    public String toString() {

        return String.format("VR=%s,idx=%d,off=%d,len=%d", vr, index, offset, length);
    }

    public byte[] toBytes(boolean bigEndian) {

        byte[] value = new byte[BDR_VALUE_LENGTH];

        // VR
        int vrCode = vr.code();
        value[0] = (byte) ((vrCode & 0xff00) >>> 8);
        value[1] = (byte) (vrCode & 0xff);

        // Reserved
        value[2] = 0x00;
        value[3] = 0x00;

        // Index, offset, and length
        if (bigEndian) {
            ByteUtils.int2bytesBE(getIndex(), value, 4);
            ByteUtils.int2bytesBE(offset, value, 8);
            ByteUtils.int2bytesBE(length, value, 12);
        } else {
            ByteUtils.int2bytesLE(getIndex(), value, 4);
            ByteUtils.int2bytesLE(offset, value, 8);
            ByteUtils.int2bytesLE(length, value, 12);
        }
        return value;
    }

    public static BulkDataReference fromBytes(byte[] value, boolean bigEndian, BulkDataSet bulkData) {

        VR vr = VR.valueOf((value[0] << 8) | value[1]);
        int index = bigEndian ? ByteUtils.bytesBE2int(value, 4) : ByteUtils.bytesLE2int(value, 4);
        int offset = bigEndian ? ByteUtils.bytesBE2int(value, 8) : ByteUtils.bytesLE2int(value, 8);
        int length = bigEndian ? ByteUtils.bytesBE2int(value, 12) : ByteUtils
                .bytesLE2int(value, 12);
        return new BulkDataReference(vr, bulkData, index, offset, length);
    }

    public VR getVR() {

        return vr;
    }

    public BulkDataSet getBulkData() {

        return bulkData;
    }

    public void setBulkData(BulkDataSet bulkData) {

        this.bulkData = bulkData;
    }

    public int getIndex() {

        return index;
    }

    public int getOffset() {

        return offset;
    }

    public int getLength() {

        return length;
    }

    public boolean isEndianToggled() {

        return isEndianToggled;
    }

    public void toggleEndian() {

        isEndianToggled = !isEndianToggled;
    }

    public byte[] getValue() {

        return getValue(length);
    }

    public byte[] getValue(int maxReadLength) {

        byte[] value = null;
        try {
            value = bulkData.readValue(index, offset, Math.min(maxReadLength, length));
        } catch (IOException e) {
            return null;
        }

        if (isEndianToggled) {
            vr.toggleEndian(value);
        }
        return value;
    }
}
