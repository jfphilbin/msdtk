/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
package org.oht.miami.msdtk.store;


/**
 * The type of the last access to the study.
 *      Read:       The last access was to read the version
 *      Write:      The last access was to create, append or modify the study
 *      Prefetch:   The last access was to prefetch the study into this cache
 *      BulkDataFlush: The last access was to flush all the BulkData objects for this study
 *      VersionsFlush: The last access was to flush all the versions of the study
 * 
 * @author James F Philbin (james.philbin@jhmi.edu)
 *
 */
enum LastAccessType {
	Read, Write, Prefetch, BulkDataFlush, VersionsFlush;
}
