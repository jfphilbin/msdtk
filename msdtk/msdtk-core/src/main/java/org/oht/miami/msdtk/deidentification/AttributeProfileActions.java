/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.deidentification;

import org.oht.miami.msdtk.deidentification.DeIdentifyUtil.ActionCode;

/**
 * Class to store the actions associated with each different de-identification
 * options
 * 
 * @author maismail (maismail@cs.jhu.edu)
 */
public class AttributeProfileActions {

    public ActionCode basicAction;
    public ActionCode retainSafePrivateAction;
    public ActionCode retainUIDsAction;
    public ActionCode retainDeviceIdentAction;
    public ActionCode retainPatientCharsAction;
    public ActionCode retainLongFullDatesAction;
    public ActionCode retainLongModifiedDatesAction;
    public ActionCode cleanDescAction;
    public ActionCode cleanStructContAction;
    public ActionCode cleanGraphAction;

    /**
     * @param basicAction
     */
    public AttributeProfileActions(final ActionCode basicAction) {
        this.basicAction = basicAction;
        retainSafePrivateAction = ActionCode.NONE;
        retainUIDsAction = ActionCode.NONE;
        retainDeviceIdentAction = ActionCode.NONE;
        retainPatientCharsAction = ActionCode.NONE;
        retainLongFullDatesAction = ActionCode.NONE;
        retainLongModifiedDatesAction = ActionCode.NONE;
        cleanDescAction = ActionCode.NONE;
        cleanStructContAction = ActionCode.NONE;
        cleanGraphAction = ActionCode.NONE;
    }

    /**
     * Non default constructor.
     * 
     * @param basic
     * @param retainSafePrivateAction
     * @param retainUIDsAction
     * @param retainDeviceIdentAction
     * @param retainPatientCharsAction
     * @param retainLongFullDatesAction
     * @param retainLongModifiedDatesAction
     * @param cleanDescAction
     * @param cleanStructContAction
     * @param cleanGraphAction
     */
    public AttributeProfileActions(final ActionCode basic,
            final ActionCode retainSafePrivateAction, final ActionCode retainUIDsAction,
            final ActionCode retainDeviceIdentAction, final ActionCode retainPatientCharsAction,
            final ActionCode retainLongFullDatesAction,
            final ActionCode retainLongModifiedDatesAction, final ActionCode cleanDescAction,
            final ActionCode cleanStructContAction, final ActionCode cleanGraphAction) {
        this.basicAction = basic;
        this.retainSafePrivateAction = retainSafePrivateAction;
        this.retainUIDsAction = retainUIDsAction;
        this.retainDeviceIdentAction = retainDeviceIdentAction;
        this.retainPatientCharsAction = retainPatientCharsAction;
        this.retainLongFullDatesAction = retainLongFullDatesAction;
        this.retainLongModifiedDatesAction = retainLongModifiedDatesAction;
        this.cleanDescAction = cleanDescAction;
        this.cleanStructContAction = cleanStructContAction;
        this.cleanGraphAction = cleanGraphAction;
    }

}