/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.studymodel;

import org.dcm4che2.data.AbstractDicomElement;
import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DateRange;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.SpecificCharacterSet;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.TransferSyntax;
import org.dcm4che2.data.VR;
import org.dcm4che2.io.DicomOutputStream;
import org.dcm4che2.util.CloseUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

public class FragmentSequenceDicomElement extends AbstractDicomElement {

    private static final long serialVersionUID = 4881192990234536161L;

    private transient List<DicomObject> items;

    private transient DicomObject parent;

    public FragmentSequenceDicomElement(int tag, VR vr, boolean bigEndian, DicomObject parent) {

        super(tag, vr, bigEndian);
        // Usually, there are 2 Fragments in a Sequence of Fragments.
        items = new ArrayList<DicomObject>(2);
        // Force hasItems() to return false
        this.parent = null;
        parent.add(this);
        this.parent = parent;
    }

    @Override
    public int vm(SpecificCharacterSet cs) {

        return items.isEmpty() ? 0 : 1;
    }

    @Override
    public int length() {

        return items.isEmpty() ? 0 : -1;
    }

    @Override
    public boolean isEmpty() {

        return items.isEmpty();
    }

    @Override
    public boolean hasItems() {

        // Return false if not yet added to a DicomObject, in order to deceive
        // BasicDicomObject.add()
        return parent != null;
    }

    @Override
    public int countItems() {

        return items.size();
    }

    @Override
    public byte[] getBytes() {

        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasDicomObjects() {

        return true;
    }

    @Override
    public boolean hasFragments() {

        return true;
    }

    @Override
    public DicomObject getDicomObject() {

        return items.isEmpty() ? null : items.get(0);
    }

    @Override
    public DicomObject getDicomObject(int index) {

        return items.get(index);
    }

    @Override
    public DicomObject removeDicomObject(int index) {

        DicomObject item = items.remove(index);
        item.setParent(null);
        updateItemPositions(index);
        return item;
    }

    @Override
    public boolean removeDicomObject(DicomObject item) {

        if (!items.remove(item)) {
            return false;
        }
        item.setParent(null);
        updateItemPositions(item.getItemPosition() - 1);
        return true;
    }

    private void updateItemPositions(int index) {

        for (int i = index, n = countItems(); i < n; i++) {
            getDicomObject(i).setItemPosition(i + 1);
        }
    }

    @Override
    public DicomObject addDicomObject(DicomObject item) {

        if (item == null) {
            throw new NullPointerException();
        }
        items.add(item);
        item.setParent(parent);
        item.setItemPosition(countItems());
        return item;
    }

    @Override
    public DicomObject addDicomObject(int index, DicomObject item) {

        if (item == null) {
            throw new NullPointerException();
        }
        items.add(index, item);
        item.setParent(parent);
        updateItemPositions(index);
        return item;
    }

    @Override
    public DicomObject setDicomObject(int index, DicomObject item) {

        if (item == null) {
            throw new NullPointerException();
        }
        items.set(index, item);
        item.setParent(parent);
        item.setItemPosition(index + 1);
        return item;
    }

    private byte[] encodeFragment(DicomObject item) {

        ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
        DicomOutputStream dicomOut = new DicomOutputStream(bytesOut);
        try {
            dicomOut.writeDataset(item, TransferSyntax.ExplicitVRLittleEndian);
        } catch (IOException e) {
            throw new RuntimeException("Unexpected error", e);
        } finally {
            CloseUtils.safeClose(dicomOut);
        }
        return bytesOut.toByteArray();
    }

    @Override
    public byte[] getFragment(int index) {

        return encodeFragment(items.get(index));
    }

    @Override
    public byte[] removeFragment(int index) {

        return encodeFragment(removeDicomObject(index));
    }

    @Override
    public boolean removeFragment(byte[] b) {

        throw new UnsupportedOperationException();
    }

    private DicomObject makeFragment(byte[] b) {

        if (b == null) {
            throw new NullPointerException();
        }
        DicomObject item = new BasicDicomObject();
        item.putBytes(Tag.Fragment, this.vr, b, this.bigEndian);
        return item;
    }

    private DicomObject makeFragment(BulkDataReference bdr) {

        if (bdr == null) {
            throw new NullPointerException();
        }
        DicomObject item = new BasicDicomObject(1);
        BulkDataDicomElement fragmentAttribute = new BulkDataDicomElement(Tag.Fragment,
                this.bigEndian, bdr);
        item.add(fragmentAttribute);
        return item;
    }

    @Override
    public byte[] addFragment(byte[] b) {

        addDicomObject(makeFragment(b));
        return b;
    }

    @Override
    public byte[] addFragment(int index, byte[] b) {

        addDicomObject(index, makeFragment(b));
        return b;
    }

    public void addFragment(BulkDataReference bdr) {

        addDicomObject(makeFragment(bdr));
    }

    public void addFragment(int index, BulkDataReference bdr) {

        addDicomObject(index, makeFragment(bdr));
    }

    @Override
    public byte[] setFragment(int index, byte[] b) {

        setDicomObject(index, makeFragment(b));
        return b;
    }

    public void setFragment(int index, BulkDataReference bdr) {

        setDicomObject(index, makeFragment(bdr));
    }

    @Override
    public short[] getShorts(boolean cache) {

        throw new UnsupportedOperationException();
    }

    @Override
    public int getInt(boolean cache) {

        throw new UnsupportedOperationException();
    }

    @Override
    public int[] getInts(boolean cache) {

        throw new UnsupportedOperationException();
    }

    @Override
    public float getFloat(boolean cache) {

        throw new UnsupportedOperationException();
    }

    @Override
    public float[] getFloats(boolean cache) {

        throw new UnsupportedOperationException();
    }

    @Override
    public double getDouble(boolean cache) {

        throw new UnsupportedOperationException();
    }

    @Override
    public double[] getDoubles(boolean cache) {

        throw new UnsupportedOperationException();
    }

    @Override
    public String getString(SpecificCharacterSet cs, boolean cache) {

        throw new UnsupportedOperationException();
    }

    @Override
    public String[] getStrings(SpecificCharacterSet cs, boolean cache) {

        throw new UnsupportedOperationException();
    }

    @Override
    public Date getDate(boolean cache) {

        throw new UnsupportedOperationException();
    }

    @Override
    public Date[] getDates(boolean cache) {

        throw new UnsupportedOperationException();
    }

    @Override
    public DateRange getDateRange(boolean cache) {

        throw new UnsupportedOperationException();
    }

    @Override
    public Pattern getPattern(SpecificCharacterSet cs, boolean ignoreCase, boolean cache) {

        throw new UnsupportedOperationException();
    }

    @Override
    public DicomElement share() {

        throw new UnsupportedOperationException();
    }

    @Override
    public DicomElement filterItems(DicomObject filter) {

        throw new UnsupportedOperationException();
    }

    @Override
    public String getValueAsString(SpecificCharacterSet cs, int truncate) {

        throw new UnsupportedOperationException();
    }

    @Override
    public void setValue(byte[] value) {

        throw new UnsupportedOperationException();
    }

    @Override
    public void setValue(Date value) {

        throw new UnsupportedOperationException();
    }

    @Override
    public void setValue(int value) {

        throw new UnsupportedOperationException();
    }

    @Override
    public void setValue(short value) {

        throw new UnsupportedOperationException();
    }

    @Override
    public void setValue(float value) {

        throw new UnsupportedOperationException();
    }

    @Override
    public void setValue(String value) {

        throw new UnsupportedOperationException();
    }

    @Override
    public void setValue(String value, SpecificCharacterSet cs) {

        throw new UnsupportedOperationException();
    }

    @Override
    protected void appendValue(StringBuffer sb, int maxValLen) {

        int size = items.size();
        if (size != 0) {
            if (size == 1) {
                sb.append("1 fragment");
            } else {
                sb.append(size).append(" fragments");
            }
        }
    }

    @Override
    protected void toggleEndian() {

        for (DicomObject item : items) {
            DicomElement fragmentAttribute = item.get(Tag.Fragment);
            fragmentAttribute.bigEndian(!this.bigEndian);
        }
    }
}
