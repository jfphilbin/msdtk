/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.store;

import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;

import org.dcm4che2.data.Tag;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * This class contains some basic information about the study that is useful for
 * displaying it.
 * 
 * @copyright
 * @author James F Philbin, Ryan Stonebraker
 */
public class StudyInfo implements Serializable {
    // TODO Expand this to return HTML or JSON for Web apps.
    // Constants

    /**
     * 
     */
    private static final long serialVersionUID = -6877089926903620379L;

    /**
     * The DICOM Study Instance UID for this study.
     */
    private final DicomUID studyInstanceUID;

    // Fields

    /**
     * The study description
     */
    private final String studyDescription;

    // TODO how to handle the accession number array
    // private final AccessionList accList;//A list of (Issuer of Accession
    // Number, Accession Number) pairs

    /**
     * The date&time, in UTC the zeroth (0th) version of the study was created
     */
    // TODO: Should field be removed? It will be present in the version history
    // object in study entry. Field should also be set to final.
    private final Date creationDate;

    // TODO decide is this is needed. Make final.
    /**
     * The data&time the study most recently had destructive modifications made
     * to it
     */
    public Date modifiedDate;

    /**
     * A string containing a comma separated list of the standard abbreviations
     * of any modality contained in this study, e.g. a Pet/CT study would have
     * â€œPT,CT"
     */
    private final String modalities;

    /**
     * The number of Series contained in the study
     */
    private final int nSeries;

    /**
     * The number of images contained in the study
     */
    private final int nImages;

    // TODO determine if these Encrypted fields are a good idea.
    /**
     * Encrypted Data: [TBD: Included in V1.0, but not encrypted until V1.1.]
     * The Universal ID Number (Peake UMRN) of the patient to whom the study
     * belongs. TODO This should be id for patient from mpi, discuss with JFP
     */
    private final String patientId;

    /**
     * The Universal ID Number of the referring physician
     */
    private final String referringPhysicianName;

    /**
     * Constructor
     * 
     * @param study
     *            A StudyModel Study object
     */
    public StudyInfo(Study study) {
        studyInstanceUID = study.getStudyInstanceUID();
        studyDescription = study.getValueForAttributeAsString(Tag.StudyDescription);
        nSeries = study.seriesCount();
        if (study.getAttribute(Tag.ModalitiesInStudy) != null) {
            modalities = study.getValueForAttributeAsString(Tag.ModalitiesInStudy).replaceAll(
                    "\\\\", ", ");
        } else {
            modalities = null;
        }
        nImages = study.frameCount();
        if (study.getAttribute(Tag.PatientID) != null) {
            patientId = study.getValueForAttributeAsString(Tag.PatientID);
        } else {
            patientId = null;
        }
        referringPhysicianName = study.getValueForAttributeAsString(Tag.ReferringPhysicianName);
        if (study.getAttribute(Tag.CreationDate) != null) {
            creationDate = study.getAttribute(Tag.CreationDate).getDate(false);
        } else {
            creationDate = null;
        }
    }

    // Getter & Setters

    /**
     * Get creation date
     * 
     * @return the creation date
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * Get modified date
     * 
     * @return the modifiedDate
     */
    public Date getModifiedDate() {
        return modifiedDate;
    }

    /**
     * Get study instance uid
     * 
     * @return DicomUID
     */
    public DicomUID getStudyInstanceUID() {
        return studyInstanceUID;
    }

    /**
     * Get study description
     * 
     * @return studyDescription
     */
    public String getStudyDescription() {
        return studyDescription;
    }

    /**
     * Get modalities
     * 
     * @return modalities
     */
    public String getModalities() {
        return modalities;
    }

    /**
     * Get number of series
     * 
     * @return n series
     */
    public int getNumberOfSeries() {
        return nSeries;
    }

    /**
     * Get number of images
     * 
     * @return nImages
     */
    public int getNumberOfImages() {
        return nImages;
    }

    /**
     * Get patient id
     * 
     * @return patient uuid
     */
    public String getPatientId() {
        return patientId;
    }

    /**
     * Get referring physician
     * 
     * @return UUID of referring physician
     */
    public String getReferringPhysician() {
        return referringPhysicianName;
    }

    // Methods

    /**
     * Get String format
     * 
     * @return String representation of study info
     */
    public String toString() {
        // TODO finalize the format
        return "StudyInfo: " + studyInstanceUID + ", " + studyDescription;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
        result = prime * result + ((modalities == null) ? 0 : modalities.hashCode());
        result = prime * result + ((modifiedDate == null) ? 0 : modifiedDate.hashCode());
        result = prime * result + nImages;
        result = prime * result + nSeries;
        result = prime * result + ((patientId == null) ? 0 : patientId.hashCode());
        result = prime * result
                + ((referringPhysicianName == null) ? 0 : referringPhysicianName.hashCode());
        result = prime * result + ((studyDescription == null) ? 0 : studyDescription.hashCode());
        result = prime * result + ((studyInstanceUID == null) ? 0 : studyInstanceUID.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        StudyInfo other = (StudyInfo) obj;
        if (creationDate == null) {
            if (other.creationDate != null) {
                return false;
            }
        } else if (!creationDate.equals(other.creationDate)) {
            return false;
        }
        if (modalities == null) {
            if (other.modalities != null) {
                return false;
            }
        } else if (!modalities.equals(other.modalities)) {
            return false;
        }
        if (modifiedDate == null) {
            if (other.modifiedDate != null) {
                return false;
            }
        } else if (!modifiedDate.equals(other.modifiedDate)) {
            return false;
        }
        if (nImages != other.nImages) {
            return false;
        }
        if (nSeries != other.nSeries) {
            return false;
        }
        if (patientId == null) {
            if (other.patientId != null) {
                return false;
            }
        } else if (!patientId.equals(other.patientId)) {
            return false;
        }
        if (referringPhysicianName == null) {
            if (other.referringPhysicianName != null) {
                return false;
            }
        } else if (!referringPhysicianName.equals(other.referringPhysicianName)) {
            return false;
        }
        if (studyDescription == null) {
            if (other.studyDescription != null) {
                return false;
            }
        } else if (!studyDescription.equals(other.studyDescription)) {
            return false;
        }
        if (studyInstanceUID == null) {
            if (other.studyInstanceUID != null) {
                return false;
            }
        } else if (!studyInstanceUID.equals(other.studyInstanceUID)) {
            return false;
        }
        return true;
    }

    public boolean contains(StudyInfo other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (modalities != null && other.modalities != null) {
            if (!containsModalities(other.modalities)) {
                return false;
            }
        }
        if (modalities == null && other.modalities != null) {
            return false;
        }
        if (nImages < other.nImages) {
            return false;
        }
        if (nSeries < other.nSeries) {
            return false;
        }
        if (studyInstanceUID == null) {
            if (other.studyInstanceUID != null) {
                return false;
            }
        } else if (!studyInstanceUID.equals(other.studyInstanceUID)) {
            return false;
        }
        return true;
    }

    private boolean containsModalities(String otherModalities) {
        List<String> otherModalityList = Arrays.asList(otherModalities.split(","));
        List<String> modalityList = Arrays.asList(modalities.split(","));
        return modalityList.containsAll(otherModalityList);
    }

}
