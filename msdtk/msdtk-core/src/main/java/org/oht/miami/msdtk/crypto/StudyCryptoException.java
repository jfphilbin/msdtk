/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.crypto;

import java.security.GeneralSecurityException;

public class StudyCryptoException extends GeneralSecurityException {

    private static final long serialVersionUID = 4399344484158503301L;

    public StudyCryptoException() {

        super();
    }

    public StudyCryptoException(String message) {

        super(message);
    }

    public StudyCryptoException(Throwable cause) {

        super(cause);
    }

    public StudyCryptoException(String message, Throwable cause) {

        super(message, cause);
    }
}
