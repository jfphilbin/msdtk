/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.store;

import static org.junit.Assert.assertTrue;

import org.oht.miami.msdtk.util.DicomUID;
import org.oht.miami.msdtk.util.TestUtils;

import org.dcm4che2.data.VR;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;

/**
 * Tests BulkDataSet read and write functionality and accuracy.
 * 
 * @author Ryan Stonebraker
 */
public class BulkDataSetTest {

    /**
     * Test data string 1
     */
    private static final byte[] TEST_DATA1 = "This is some data".getBytes();

    /**
     * Test data string 2
     */
    private static final byte[] TEST_DATA2 = "This is some more binary data".getBytes();

    /**
     * Test data string 3
     */
    private static final byte[] TEST_DATA3 = "Even more binary data for testing".getBytes();

    private Store store;

    private BulkDataSet bulkDataSet;

    @Before
    public void setUp() {
        store = StoreFactory.createStore("testStoreFS.properties");
        bulkDataSet = null;
    }

    @After
    public void tearDown() throws IOException {
        TestUtils.cleanStore(store);
    }

    /**
     * Tests writing a series of test data and reading them back out compared
     * against the original.
     * 
     * @throws IOException
     */
    @Test
    public void testReadWriteValues() throws IOException {
        bulkDataSet = new BulkDataSet(new DicomUID(), store);

        int offset1, offset2, offset3;
        try {
            offset1 = bulkDataSet.appendValue(VR.LO, TEST_DATA1).getOffset();
            offset2 = bulkDataSet.appendValue(VR.LO, TEST_DATA2).getOffset();
            offset3 = bulkDataSet.appendValue(VR.LO, TEST_DATA3).getOffset();
        } finally {
            bulkDataSet.close();
        }

        byte[] result2 = bulkDataSet.readValue(0, offset2, TEST_DATA2.length);
        byte[] result1 = bulkDataSet.readValue(0, offset1, TEST_DATA1.length);
        byte[] result3 = bulkDataSet.readValue(0, offset3, TEST_DATA3.length);
        assertTrue(Arrays.equals(TEST_DATA1, result1));
        assertTrue(Arrays.equals(TEST_DATA2, result2));
        assertTrue(Arrays.equals(TEST_DATA3, result3));
    }

}
