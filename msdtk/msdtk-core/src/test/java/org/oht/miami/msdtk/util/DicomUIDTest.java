/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * Tests for the DicomUID object.
 * 
 * @author Josh Stephens (joshua.stephens@harris.com)
 *
 */
public class DicomUIDTest {

    private final String exceptionMessage = "Illegal Study Instance UID:  ";

    @Test
    /**
     * Tests that a valid DICOM UID creates the DicomUID successfully.
     */
    public void testValidUID() {
        String validUID = "1.2.250.1.59.453.859.781157385.1640.1290623935.3.3.816";
        DicomUID uid = new DicomUID(validUID);
        assertEquals(uid.toString(), validUID);
    }

    @Test
    /**
     * Tests that an Invalid UID with Repeating Decimals throws the correct exception.
     */
    public void testInvalidUIDRepeatingDecimals() {
        String invalidUIDRepeatingDecimals = "1...2.250.1.59.453.859.781157385.1640.1290623935.3.3.816";
        try {
            new DicomUID(invalidUIDRepeatingDecimals);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), exceptionMessage + invalidUIDRepeatingDecimals);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    /**
     * Tests that an Invalid UID with prepended decimals throws the correct exception.
     */
    public void testInvalidUIDPrependedDecimals() {
        String invalidUIDPrependedDecimals = "...1.2.250.1.59.453.859.781157385.1640.1290623935.3.3.816";
        try {
            new DicomUID(invalidUIDPrependedDecimals);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), exceptionMessage + invalidUIDPrependedDecimals);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    /**
     * Tests that an Invalid UID with trailing decimals throws the correct exception.
     */
    public void testInvalidUIDTrailingDecimals() {
        String invalidUIDTrailingDecimals = "1.2.250.1.59.453.859.781157385.1640.1290623935.3.3.816...";
        try {
            new DicomUID(invalidUIDTrailingDecimals);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), exceptionMessage + invalidUIDTrailingDecimals);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    /**
     * Tests that an Invalid UID with Invalid characters throws the correct exception.
     */
    public void testInvalidUIDInvalidCharacters() {
        String invalidUIDInvalidCharacters = "1.2.250.1.59.453.859.781157385.1640.!290623935.3.3.816";
        try {
            new DicomUID(invalidUIDInvalidCharacters);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), exceptionMessage + invalidUIDInvalidCharacters);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    /**
     * Tests that an Invalid UID with alphabetic characters throws the correct exception.
     */
    public void testInvalidUIDAlphaCharacters() {
        String invalidUIDAlphaCharacters = "a.b.c.d";
        try {
            new DicomUID(invalidUIDAlphaCharacters);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), exceptionMessage + invalidUIDAlphaCharacters);
        } catch (Exception e) {
            fail();
        }
    }

}
