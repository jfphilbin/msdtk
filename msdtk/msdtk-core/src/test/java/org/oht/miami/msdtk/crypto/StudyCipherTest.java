/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.crypto;

import static org.junit.Assert.assertEquals;

import org.oht.miami.msdtk.util.DicomUID;

import io.netty.buffer.ByteBuf;

import java.util.UUID;

public abstract class StudyCipherTest {

    private static final DicomUID TEST_STUDY_IUID = new DicomUID();

    private static final UUID TEST_UUID = UUID.randomUUID();

    private static final String UNENCRYPTED_TEXT = "blah blah blah blah";

    private static final byte[] UNENCRYPTED_BYTES = UNENCRYPTED_TEXT.getBytes();

    protected final StudyCipher cipher;

    protected StudyCipherTest(StudyCipher cipher) {

        this.cipher = cipher;
    }

    private ByteBuf prepareData(boolean compress) {

        int unencryptedSize = UNENCRYPTED_BYTES.length;
        ByteBuf unencrypted = cipher.allocateBufferForUnencryptedData(unencryptedSize, compress);
        unencrypted.writeBytes(UNENCRYPTED_BYTES);
        return unencrypted;
    }

    protected void testEncryptString(boolean compress) throws StudyCryptoException {

        ByteBuf unencrypted = prepareData(compress);
        cipher.initEncrypt(TEST_STUDY_IUID, compress, TEST_UUID, UNENCRYPTED_BYTES.length);
        ByteBuf encrypted = cipher.doFinal(unencrypted);
        ByteBuf unauthenticated = cipher.allocateBufferForEncryptedData(encrypted.readableBytes());
        unauthenticated.writeBytes(encrypted);

        cipher.initAuthenticate(TEST_STUDY_IUID);
        ByteBuf authenticated = cipher.doFinal(unauthenticated);

        cipher.initDecrypt(TEST_STUDY_IUID);
        ByteBuf decrypted = cipher.doFinal(authenticated);

        byte[] decryptedBytes = new byte[decrypted.readableBytes()];
        decrypted.readBytes(decryptedBytes);
        String decryptedText = new String(decryptedBytes);
        assertEquals(UNENCRYPTED_TEXT, decryptedText);
    }
}
