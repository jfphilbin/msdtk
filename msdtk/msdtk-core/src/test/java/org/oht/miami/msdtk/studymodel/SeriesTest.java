/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.studymodel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.util.DicomUID;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.SimpleDicomElement;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Test the behavior of the Series object when facing modification
 * 
 * @author Jiefeng Zhai
 */
public class SeriesTest {

    private Study studyNormalized = null;

    private Series series = null;

    private Store store = StoreFactory.createStore("testStoreFS.properties");

    @Before
    public void setUp() throws IOException {
        final String STUDY_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.1166562673.14401";
        DicomElement studyUID = new SimpleDicomElement(Tag.StudyInstanceUID, VR.UI, true,
                STUDY_UID_VALUE.getBytes(), null);
        final String SERIES_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.1166562673.14401";
        DicomElement seriesUID = new SimpleDicomElement(Tag.SeriesInstanceUID, VR.UI, true,
                SERIES_UID_VALUE.getBytes(), null);
        final String SOP_UID_VALUE = "1.3.6.1.4.1.5962.1.1.10.2.1.1166562673.14401";
        DicomElement sopUID = new SimpleDicomElement(Tag.SOPInstanceUID, VR.UI, true,
                SOP_UID_VALUE.getBytes(), null);
        final String OPERATOR_VALUE = "Jiefeng^Zhai";
        DicomElement operator = new SimpleDicomElement(Tag.OperatorsName, VR.PN, true,
                OPERATOR_VALUE.getBytes(), null);

        DicomObject obj = new BasicDicomObject();
        obj.add(sopUID);
        obj.add(studyUID);
        obj.add(seriesUID);

        studyNormalized = new Study(store, new DicomUID(STUDY_UID_VALUE), true, obj);

        series = studyNormalized.getSeries(SERIES_UID_VALUE);
        series.putAttribute(operator);
    }

    @After
    public void tearDown() {
        studyNormalized = null;
        series = null;
    }

    /**
     * Add some new attributes and the number of attributes should remain
     * consistent
     */
    @Test
    public void testAddAttributes() {
        final String value01 = "1.2.840.10008.5.1.4.1.1.2";
        DicomElement attr01 = new SimpleDicomElement(Tag.PatientPosition, VR.CS, true,
                value01.getBytes(), null);
        final String value02 = "CT";
        DicomElement attr02 = new SimpleDicomElement(Tag.Modality, VR.CS, true, value02.getBytes(),
                null);

        assertEquals(series.getNumberOfSeriesAttributes(), 2);

        try {
            series.putAttribute(attr01);
            series.putAttribute(attr02);
        } catch (RuntimeException e) {
            fail("unexpected exception");
        }

        assertEquals(series.getNumberOfSeriesAttributes(), 4);
    }

    /**
     * Delete some attributes and the number of attributes should remain
     * consistent
     */
    @Test
    public void testDeleteAttributes() {
        assertEquals(series.getNumberOfSeriesAttributes(), 2);

        series.removeAttribute(Tag.OperatorsName);

        assertEquals(series.getNumberOfSeriesAttributes(), 1);
    }
}
