/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.conversion;

import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;
import org.oht.miami.msdtk.util.Study2Dicom;
import org.oht.miami.msdtk.util.TestUtils;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomInputStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * Tests the GPB conversion for accuracy. Dumps the contents of a study before
 * and after GPB conversion to plain text in order to be compared externally.
 * 
 * @author shu18
 */

// TODO Remove the @Ignore annotation if we decide to re-enable this test
@Ignore
@Deprecated
public class GpbTest {

    private static final String INPUT_ROOT = "C:/SMALLMR_MFFN/";

    private static final String OUTPUT_ROOT = "target/test-out/GPB";

    private Store store;

    @Before
    public void setUp() throws IOException {
        store = StoreFactory.createStore("testStoreFS.properties");
    }

    @After
    public void tearDown() throws IOException {

        TestUtils.cleanStore(store);
    }

    /**
     * Builds the study model from a Dicom file
     * 
     * @param in
     *            the path of the Dicom files directory
     * @param out
     *            the path to the output directory
     * @param studyName
     *            the prefix name for the study
     * @param normalize
     *            whether or not to normalize
     * @return the newly constructed study model
     * @throws IOException
     */
    @Deprecated
    private Study readDicom(String in, String out, String studyName, boolean normalize)
            throws IOException {
        Study study = null;
        File directory = new File(in);
        File[] dicomFiles = directory.listFiles();
        DicomObject dcmObj;

        File outDir = new File(out);
        if (!outDir.exists()) {
            outDir.mkdirs();
        }
        for (File file : dicomFiles) {
            DicomInputStream inputStream = null;
            if (file.toString().endsWith(".dcm")) {
                try {
                    inputStream = new DicomInputStream(file);
                    dcmObj = inputStream.readDicomObject();
                    if (null == study)
                        study = new Study(store, new DicomUID(
                                dcmObj.getString(Tag.StudyInstanceUID)), normalize, dcmObj);
                    else
                        study.addDicomObject(dcmObj);
                } finally {
                    inputStream.close();
                }
            }
        }
        return study;
    }

    /**
     * Tests writing to GPB files and checks for accuracy by dumping contents
     * before and after conversions (without normalization)
     * 
     * @throws IOException
     */
    @Test
    public void testGPBWithoutNormalization() throws IOException {
        Study study = readDicom(INPUT_ROOT, OUTPUT_ROOT + "no-normalize/", "original", true);

        // Get the Java runtime
        /*
         * Runtime runtime = Runtime.getRuntime();
         * System.out.println("Total memory = "+ runtime.totalMemory());
         * 
         * long memory = runtime.totalMemory() - runtime.freeMemory();
         * System.out.println("Used memory is bytes: " + memory); // Run the
         * garbage collector runtime.gc(); // Calculate the used memory
         * 
         * memory = runtime.totalMemory() - runtime.freeMemory();
         * System.out.println("Used memory is megabytes: " + memory);
         */
        // System.out.println(study.getStudyInstanceUID());

        // dump contents of study to get a base for comparison
        study.dumpStudyContents(OUTPUT_ROOT
                + "no-normalize/beforeWithoutNormalization_enhancedNormalization");
        try {
            // Mint2DicomConverter.Mint2MultiFrameDicom(study, new
            // File(OUTPUT_ROOT+"/no-normalize/"));
            Study2Dicom.study2SingleFrameDicom(study, new File(OUTPUT_ROOT + "/no-normalize/"),
                    true);
            // study.writeToGPB();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // convert MINT study model into GPB
        // study.writeToGPB();

        // Study result = readDicom(OUTPUT_ROOT + "no-normalize/", OUTPUT_ROOT +
        // "no-normalize/", "new", true);

        // read from newly created GPB file into study model
        // InputStream in = new FileInputStream(OUTPUT_ROOT +
        // "no-normalize/original.gpb");
        // MintGPB.StudyGPB data = MintGPB.StudyGPB.parseFrom(in);
        // Study result = new Study(data, OUTPUT_ROOT +
        // "no-normalize/original");

        // result.dumpStudyContents(OUTPUT_ROOT +
        // "no-normalize/afterWithNormalization");
        // dump contents of newly formed study model for comparison with base
        // contents
        // Mint2DicomConverter.Mint2CompositeDicom(result, new
        // File(OUTPUT_ROOT+"/normalize/"));
    }

    /**
     * Tests writing to GPB files and checks for accuracy by dumping contents
     * before and after conversions (with normalization)
     * 
     * @throws IOException
     */
    /*
     * @Test public void testGPBWithNormalization() throws IOException { Study
     * study = readDicom(INPUT_ROOT, OUTPUT_ROOT + "normalize/", "original",
     * true); try { //dump contents of study to get a base for comparison
     * study.dumpStudyContents(OUTPUT_ROOT +
     * "normalize/beforeWithNormalization"); //convert MINT study model into GPB
     * study.writeToGPB(); } catch (IOException e) { e.printStackTrace(); }
     * Mint2DicomConverter.Mint2MultiFrameDicom(study, new
     * File(OUTPUT_ROOT+"/normalize/"));
     * Mint2DicomConverter.Mint2MultiSeriesDicom(study, new
     * File(OUTPUT_ROOT+"/normalize/")); //read from newly created GPB file into
     * study model InputStream in = new FileInputStream(OUTPUT_ROOT +
     * "normalize/original.gpb"); MintGPB.StudyGPB data =
     * MintGPB.StudyGPB.parseFrom(in); Study result = new Study(data, null);
     * 
     * //dump contents of newly formed study model for comparison with base
     * contents result.dumpStudyContents(OUTPUT_ROOT +
     * "normalize/afterWithNormalization"); }
     */
}
