/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.studymodel;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.util.TestUtils;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.SimpleDicomElement;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Tests the MS-DICOM Series data model functionality and accuracy.
 * 
 * @author Sam Hu
 * @author Raphael Yu Ning
 */
public class OldSeriesTest {

    private Study studyNormalized = null;

    private Series seriesNormalized = null;

    private Store store;

    private final static String TEST_STUDY_NAME = "MINT10";
    private final static String TEST_SERIES_IUID = "2.16.840.1.114255.393386351.1568457295.5012.1";

    @Before
    public void setUp() throws IOException {

        store = StoreFactory.createStore("testStoreFS.properties");
        studyNormalized = TestUtils.readStudyFromResource(store, TEST_STUDY_NAME);
        seriesNormalized = studyNormalized.getSeries(TEST_SERIES_IUID);
    }

    @After
    public void tearDown() throws IOException {
        TestUtils.cleanStore(store);
    }

    /**
     * Compares the expected series IUID with what was loaded into the data
     * model.
     */
    @Test
    public void testGetSeriesInstanceUid() {

        testGetSeriesInstanceUid(seriesNormalized);
    }

    private static void testGetSeriesInstanceUid(Series series) {

        assertEquals("2.16.840.1.114255.393386351.1568457295.5012.1", series.getSeriesInstanceUID());
    }

    /**
     * Tests putting attributes into a series. <br/>
     * Should throw an exception if an attribute with the same tag already
     * exists in the series.
     */
    @Test
    public void testPutAttribute() {

        testPutAttribute(seriesNormalized);
    }

    private static void testPutAttribute(Series series) {

        final int TEST_TAG = 0xffffffff;
        final String TEST_VALUE = "test attribute";
        final String TEST_NEW_VALUE = "another test attribute";

        series.putAttribute(new SimpleDicomElement(TEST_TAG, VR.UN, true, TEST_VALUE.getBytes(),
                null));
        DicomElement attr = series.getAttribute(TEST_TAG);
        assertEquals(TEST_VALUE, new String(attr.getBytes()));

        try {
            series.putAttribute(new SimpleDicomElement(TEST_TAG, VR.AE, true, TEST_NEW_VALUE
                    .getBytes(), null));
            fail("Missing expected exception");
        } catch (IllegalArgumentException e) {
            DicomElement newAttr = series.getAttribute(TEST_TAG);
            assertEquals(TEST_VALUE, new String(newAttr.getBytes()));
        }
    }

    /**
     * Tests getting the value of an attribute in the series.
     */
    @Test
    public void testGetValue() {
        testGetValue(seriesNormalized);
    }

    private static void testGetValue(Series series) {

        final int TEST_TAG = 0xffffffff;
        final String TEST_VALUE = "test attribute";

        assertNull(series.getValueForAttributeAsString(TEST_TAG));
        assertNull(series.getValueForAttribute(TEST_TAG));

        series.putAttribute(new SimpleDicomElement(TEST_TAG, VR.UN, true, TEST_VALUE.getBytes(),
                null));
        assertEquals(TEST_VALUE, series.getValueForAttributeAsString(TEST_TAG));
        assertArrayEquals(TEST_VALUE.getBytes(), series.getValueForAttribute(TEST_TAG));
    }

    /**
     * Tests removing an attribute from the series.
     */
    @Test
    public void testRemoveAttribute() {

        testRemoveAttribute(seriesNormalized);
    }

    private static void testRemoveAttribute(Series series) {

        final int TEST_TAG = 0xffffffff;
        final String TEST_VALUE = "test attribute";

        series.putAttribute(new SimpleDicomElement(TEST_TAG, VR.UN, true, TEST_VALUE.getBytes(),
                null));
        assertNotNull(series.getAttribute(TEST_TAG));

        series.removeAttribute(TEST_TAG);
        assertNull(series.getAttribute(TEST_TAG));
    }

    /**
     * Compares the expected number of instances with what the data model has.
     */
    @Test
    public void testGetInstanceCount() {

        testGetInstanceCount(seriesNormalized);
    }

    private static void testGetInstanceCount(Series series) {

        assertEquals(1, series.getInstanceCount());
    }

    /**
     * Tests adding an instance to the series.
     */
    @Test
    public void testAddInstance() {

        testAddInstance(seriesNormalized);
    }

    private static void testAddInstance(Series series) {

        final String TEST_SOP_IUID = "test instance";

        Instance instance = new Instance(series);
        instance.putAttribute(new SimpleDicomElement(Tag.SOPInstanceUID, VR.UI, false,
                TEST_SOP_IUID.getBytes(), null));

        int startingInstanceCount = series.getInstanceCount();
        series.addInstance(instance);

        assertEquals(startingInstanceCount + 1, series.getInstanceCount());
        assertNotNull(series.getInstance(TEST_SOP_IUID));
    }

    /**
     * Tests getting a specific instance from the series.
     */
    @Test
    public void testGetInstance() {

        testGetInstance(seriesNormalized);
    }

    private static void testGetInstance(Series series) {

        assertNotNull(series.getInstance("2.16.840.1.114255.393386351.1568457295.17895.5"));
        assertNull(series.getInstance("this instance does not exist"));
    }

    /**
     * Tests removing a specific instance from the series.
     */
    @Test
    public void testRemoveInstance() {
        testRemoveInstance(seriesNormalized);
    }

    private static void testRemoveInstance(Series series) {

        final String TEST_SOP_IUID = "2.16.840.1.114255.393386351.1568457295.17895.5";

        series.removeInstance(TEST_SOP_IUID);
        assertNull(series.getInstance(TEST_SOP_IUID));

        series.removeInstance("this instance does not exist");
    }

    /**
     * Tests getting the number of frames per series.
     */
    @Test
    public void testGetNumberOfFrames() {
        // TODO no number of frames data for current DICOM file used for testing
    }

    /**
     * Compares expected elements in the DICOM file with those loaded into the
     * Series model.
     */
    @Test
    public void testSeriesAttributes() {

        testSeriesAttributes(seriesNormalized);
    }

    private static void testSeriesAttributes(Series series) {

        boolean isNormalized = series.getStudyParent().isNormalized();
        DicomElement attr = null;

        attr = series.getAttribute(Tag.SeriesDate);
        if (isNormalized) {
            // this attribute is moved to study level during normalization
            assertNull(attr);
        } else {
            // FIXME: SpecificCharacterSet should not always be null
            assertEquals("20100408", attr.getValueAsString(null, 0));
            assertEquals("DA", attr.vr().toString());
        }

        attr = series.getAttribute(Tag.SeriesTime);
        assertEquals("073007.0000", attr.getValueAsString(null, 0));
        assertEquals("TM", attr.vr().toString());

        attr = series.getAttribute(Tag.Modality);
        if (isNormalized) {
            // this attribute is moved to study level during normalization
            assertNull(attr);
        } else {
            assertEquals("CR", attr.getValueAsString(null, 0));
            assertEquals("CS", attr.vr().toString());
        }

        attr = series.getAttribute(Tag.SeriesDescription);
        assertEquals("W Chest lat", attr.getValueAsString(null, 0));
        assertEquals("LO", attr.vr().toString());

        attr = series.getAttribute(Tag.PerformingPhysicianName);
        if (isNormalized) {
            // this attribute is moved to study level during normalization
            assertNull(attr);
        } else {
            assertEquals("PERFORMING^PHYS^^^", attr.getValueAsString(null, 0));
            assertEquals("PN", attr.vr().toString());
        }

        attr = series.getAttribute(Tag.OperatorsName);
        if (isNormalized) {
            // this attribute is moved to study level during normalization
            assertNull(attr);
        } else {
            assertEquals("OPERATOR", attr.getValueAsString(null, 0));
            assertEquals("PN", attr.vr().toString());
        }

        attr = series.getAttribute(Tag.BodyPartExamined);
        if (isNormalized) {
            // this attribute is moved to study level during normalization
            assertNull(attr);
        } else {
            assertEquals("CS", attr.vr().toString());
        }

        attr = series.getAttribute(Tag.ProtocolName);
        assertEquals("W Chest lat", attr.getValueAsString(null, 0));
        assertEquals("LO", attr.vr().toString());

        attr = series.getAttribute(Tag.PatientPosition);
        if (isNormalized) {
            // this attribute is moved to study level during normalization
            assertNull(attr);
        } else {
            assertEquals("CS", attr.vr().toString());
        }

        attr = series.getAttribute(Tag.SeriesInstanceUID);
        assertEquals("2.16.840.1.114255.393386351.1568457295.5012.1",
                attr.getValueAsString(null, 0));
        assertEquals("UI", attr.vr().toString());

        attr = series.getAttribute(Tag.SeriesNumber);
        assertEquals("2", attr.getValueAsString(null, 0));
        assertEquals("IS", attr.vr().toString());

        attr = series.getAttribute(Tag.Laterality);
        if (isNormalized) {
            // this attribute is moved to study level during normalization
            assertNull(attr);
        } else {
            assertEquals("CS", attr.vr().toString());
        }

        attr = series.getAttribute(Tag.PerformedProcedureStepStartDate);
        if (isNormalized) {
            // this attribute is moved to study level during normalization
            assertNull(attr);
        } else {
            assertEquals("20100408", attr.getValueAsString(null, 0));
            assertEquals("DA", attr.vr().toString());
        }

        attr = series.getAttribute(Tag.PerformedProcedureStepStartTime);
        if (isNormalized) {
            // this attribute is moved to study level during normalization
            assertNull(attr);
        } else {
            assertEquals("072843.000", attr.getValueAsString(null, 0));
            assertEquals("TM", attr.vr().toString());
        }

        attr = series.getAttribute(Tag.PerformedProcedureStepID);
        if (isNormalized) {
            // this attribute is moved to study level during normalization
            assertNull(attr);
        } else {
            assertEquals("6993800", attr.getValueAsString(null, 0));
            assertEquals("SH", attr.vr().toString());
        }

        attr = series.getAttribute(Tag.PerformedProcedureStepDescription);
        if (isNormalized) {
            // this attribute is moved to study level during normalization
            assertNull(attr);
        } else {
            assertEquals("CHEST PA AND LATERAL", attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = series.getAttribute(Tag.PerformedProtocolCodeSequence);
        if (isNormalized) {
            // this attribute is moved to study level during normalization
            assertNull(attr);
        } else {
            assertEquals("SQ", attr.vr().toString());
        }

        attr = series.getAttribute(Tag.RequestAttributesSequence);
        if (isNormalized) {
            // this attribute is moved to study level during normalization
            assertNull(attr);
        } else {
            assertEquals("SQ", attr.vr().toString());
        }
    }
}
