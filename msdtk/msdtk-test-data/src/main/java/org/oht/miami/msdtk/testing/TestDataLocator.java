package org.oht.miami.msdtk.testing;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Helps locate test studies and datasets.
 * 
 * @author Raphael Yu Ning
 */
public class TestDataLocator {

    private static final String CONFIG_FILE = "org/oht/miami/msdtk/testing/TestDataLocator.properties";

    private Properties config = new Properties();

    public TestDataLocator() throws IOException {

        loadConfig();
    }

    private void loadConfig() throws IOException {

        InputStream configIn = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream(CONFIG_FILE);

        if (configIn == null) {
            throw new FileNotFoundException("Could not open " + CONFIG_FILE);
        }

        config.load(configIn);
        configIn.close();
    }

    public List<String> locateStudy(String studyName) {

        List<String> studyResources = new ArrayList<String>();
        String configKeyPrefix = "studies." + studyName + ".file";

        for (int i = 1;; i++) {
            String configKey = configKeyPrefix + i;
            String configValue = config.getProperty(configKey);

            if (configValue == null) {
                break;
            } else {
                studyResources.add(configValue);
            }
        }

        if (studyResources.isEmpty()) {
            return null;
        } else {
            return studyResources;
        }
    }

    public File locateDataset(String datasetName) {

        String datasetDirPath = config.getProperty("datasets." + datasetName + ".dir");

        return (datasetDirPath == null) ? null : new File(datasetDirPath);
    }

    public File locateOutput() {

        String outputDirPath = config.getProperty("results.root.dir");

        return (outputDirPath == null) ? null : new File(outputDirPath);
    }
}
