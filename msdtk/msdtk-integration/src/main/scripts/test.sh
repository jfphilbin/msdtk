if [ $# -lt 2 ]
then
  echo "Usage: $0 <input_directory> <num_iterations>"
  exit 1
fi

TEMP=./temp
for (( i = 0; i < $2; i++))
do    

  if [ ! -d $TEMP ]
  then
    #create temporary directory for multiframe files
    mkdir $TEMP
  fi

  for f in $1/*
  do
    CURR_STUDY=`basename $f`
    echo $CURR_STUDY 

    if [ ! -d "./out/$CURR_STUDY/" ]
    then
	mkdir ./out/$CURR_STUDY
    fi
  
    #create D-SFC Multi-File
    echo Writing Composite Multi-File...
    java -Xms8g -Xmx8g -jar mint-integration-2.0.jar $f $TEMP/Comp write_composite no-normalize >> ./out/$CURR_STUDY/write_comp_no-norm.txt

    #create D-MF
    echo Writing D-MF...
    java -Xms8g -Xmx8g -jar mint-integration-2.0.jar $f $TEMP/D-MF write_series_multiframe no-normalize >> ./out/$CURR_STUDY/write_MF.txt
 
    #create D-MFSN
    echo Writing D-MFSN...
    java -Xms8g -Xmx8g -jar mint-integration-2.0.jar $f $TEMP/D-MFSN write_series_multiframe normalize >> ./out/$CURR_STUDY/write_MFSN.txt

    #create D-MFFN
    echo Writing D-MFFN...
    java -Xms8g -Xmx8g -jar mint-integration-2.0.jar $f $TEMP/D-MFFN write_study_multiframe normalize >> ./out/$CURR_STUDY/write_MFFN.txt
 
 

    #read D-SFC Multi-File
    echo Reading Composite Mult-File...
    java -Xms16g -Xmx16g -jar mint-integration-2.0.jar $f ./out read_dcmObj no-normalize >> ./out/$CURR_STUDY/read_comp_dcmObj.txt

    #read D-MF
    echo Reading D-MF...
    java -Xms16g -Xmx16g -jar mint-integration-2.0.jar $TEMP/D-MF/$CURR_STUDY/ ./out read_dcmObj no-normalize >> ./out/$CURR_STUDY/read_MF_dcmObj.txt
 
    #read D-MFSN
    echo Reading D-MFSN...
    java -Xms16g -Xmx16g -jar mint-integration-2.0.jar $TEMP/D-MFSN/$CURR_STUDY/ ./out read_dcmObj no-normalize >> ./out/$CURR_STUDY/read_MFSN_dcmObj.txt
 
    #read D-MFFN
    echo Reading D-MFFN...
    java -Xms16g -Xmx16g -jar mint-integration-2.0.jar $TEMP/D-MFFN/$CURR_STUDY/ ./out read_dcmObj no-normalize >> ./out/$CURR_STUDY/read_MFFN_dcmObj.txt



    #read SFC without normalization
    echo Reading Composite without Normalization
    java -Xms8g -Xmx8g -jar mint-integration-2.0.jar $f ./out read_dicom no-normalize >> ./out/$CURR_STUDY/read_comp_no-norm.txt

    #read SFC with normalization
    echo Reading Composite with Normalization  
    java -Xms8g -Xmx8g -jar mint-integration-2.0.jar $f ./out read_dicom normalize >> ./out/$CURR_STUDY/read_comp_normal.txt

  done
  rm -r $TEMP
done
exit $?
