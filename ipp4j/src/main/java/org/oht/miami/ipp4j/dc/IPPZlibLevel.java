/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.ipp4j.dc;

/**
 * Definitions of zlib compression levels.
 * 
 * @author Raphael Yu Ning
 */
public class IPPZlibLevel {

    public static final int NO_COMPRESSION = 0;

    public static final int BEST_SPEED = 1;

    public static final int BEST_COMPRESSION = 9;

    public static final int DEFAULT_COMPRESSION = -1;
}
