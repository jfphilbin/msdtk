/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.ipp4j.core;

/**
 * JNA interface to the IppStatus enum (see ippdefs.h).
 * 
 * @author Raphael Yu Ning
 */
public class IPPStatus {

    /**
     * Incorrect value for string length.
     */
    public static final int LENGTH_ERR = -119;

    /**
     * Context parameter does not match the operation.
     */
    public static final int CONTEXT_MATCH_ERR = -17;

    /**
     * Null pointer error.
     */
    public static final int NULL_PTR_ERR = -8;

    /**
     * Incorrect value for data size.
     */
    public static final int SIZE_ERR = -6;

    /**
     * Not enough memory for the operation.
     */
    public static final int NO_MEM_ERR = -4;

    /**
     * No errors.
     */
    public static final int NO_ERR = 0;

    /**
     * No operation has been executed.
     */
    public static final int NO_OPERATION = 1;
}
