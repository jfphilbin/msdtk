/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.ipp4j.cp;

import org.oht.miami.ipp4j.core.IPPLibraryVersion;
import org.oht.miami.ipp4j.core.IPPStatus;
import org.oht.miami.ipp4j.util.DirectBuffers;

import java.nio.ByteBuffer;

import com.sun.jna.Native;
import com.sun.jna.Platform;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

/**
 * JNA interface to the IPP Cryptography library (see ippcp.h).
 * 
 * @author Raphael Yu Ning
 */
public class IPPCrypto {

    static {

        Native.register(Platform.isWindows() ? "ippcp-7.1" : "ippcp");
    }

    public static native IPPLibraryVersion ippcpGetLibVersion();

    public static native int ippsRijndael128GCMGetSizeManaged(int flag, IntByReference pSize);

    public static void rijndael128GCMGetSizeManaged(IPPAESGCMBehavior behavior, IntByReference pSize)
            throws IPPCryptoException {

        int status = ippsRijndael128GCMGetSizeManaged(behavior.getValue(), pSize);
        if (status != IPPStatus.NO_ERR) {
            throw new IPPCryptoException(status);
        }
    }

    public static native int ippsRijndael128GCMInitManaged(int flag, Pointer pKey, int keyLen,
            Pointer pState);

    public static void rijndael128GCMInitManaged(IPPAESGCMBehavior behavior, Pointer pKey,
            IPPAESKeyLength keyLength, Pointer pState) throws IPPCryptoException {

        int status = ippsRijndael128GCMInitManaged(behavior.getValue(), pKey, keyLength.getValue(),
                pState);
        if (status != IPPStatus.NO_ERR) {
            throw new IPPCryptoException(status);
        }
    }

    public static native int ippsRijndael128GCMReset(Pointer pState);

    public static void rijndael128GCMReset(Pointer pState) throws IPPCryptoException {

        int status = ippsRijndael128GCMReset(pState);
        if (status != IPPStatus.NO_ERR) {
            throw new IPPCryptoException(status);
        }
    }

    public static native int ippsRijndael128GCMStart(Pointer pIV, int ivLen, Pointer pAAD,
            int aadLen, Pointer pState);

    public static void rijndael128GCMStart(Pointer pIV, int ivLen, Pointer pAAD, int aadLen,
            Pointer pState) throws IPPCryptoException {

        int status = ippsRijndael128GCMStart(pIV, ivLen, pAAD, aadLen, pState);
        if (status != IPPStatus.NO_ERR) {
            throw new IPPCryptoException(status);
        }
    }

    public static native int ippsRijndael128GCMEncrypt(Pointer pSrc, Pointer pDst, int len,
            Pointer pState);

    public static void rijndael128GCMEncrypt(ByteBuffer src, ByteBuffer dst, Pointer pState)
            throws IPPCryptoException {

        Pointer pSrc = DirectBuffers.getPointer(src);
        Pointer pDst = DirectBuffers.getPointer(dst);
        int len = src.remaining();
        int status = ippsRijndael128GCMEncrypt(pSrc, pDst, len, pState);
        if (status != IPPStatus.NO_ERR) {
            throw new IPPCryptoException(status);
        }
        dst.limit(dst.position() + len);
    }

    public static ByteBuffer rijndael128GCMEncrypt(ByteBuffer src, Pointer pState)
            throws IPPCryptoException {

        ByteBuffer dst = src.duplicate();
        Pointer pSrc = DirectBuffers.getPointer(src);
        int len = src.remaining();
        int status = ippsRijndael128GCMEncrypt(pSrc, pSrc, len, pState);
        if (status != IPPStatus.NO_ERR) {
            throw new IPPCryptoException(status);
        }
        return dst;
    }

    public static native int ippsRijndael128GCMDecrypt(Pointer pSrc, Pointer pDst, int len,
            Pointer pState);

    public static void rijndael128GCMDecrypt(ByteBuffer src, ByteBuffer dst, Pointer pState)
            throws IPPCryptoException {

        Pointer pSrc = DirectBuffers.getPointer(src);
        Pointer pDst = DirectBuffers.getPointer(dst);
        int len = src.remaining();
        int status = ippsRijndael128GCMDecrypt(pSrc, pDst, len, pState);
        if (status != IPPStatus.NO_ERR) {
            throw new IPPCryptoException(status);
        }
        dst.limit(dst.position() + len);
    }

    public static ByteBuffer rijndael128GCMDecrypt(ByteBuffer src, Pointer pState)
            throws IPPCryptoException {

        ByteBuffer dst = src.duplicate();
        Pointer pSrc = DirectBuffers.getPointer(src);
        int len = src.remaining();
        int status = ippsRijndael128GCMDecrypt(pSrc, pSrc, len, pState);
        if (status != IPPStatus.NO_ERR) {
            throw new IPPCryptoException(status);
        }
        return dst;
    }

    public static native int ippsRijndael128GCMGetTag(Pointer pDstTag, int tagLen, Pointer pState);

    public static void rijndael128GCMGetTag(Pointer pDstTag, int tagLen, Pointer pState)
            throws IPPCryptoException {

        int status = ippsRijndael128GCMGetTag(pDstTag, tagLen, pState);
        if (status != IPPStatus.NO_ERR) {
            throw new IPPCryptoException(status);
        }
    }

    public static native int ippsRijndael128GCMProcessIV(Pointer pIV, int ivLen, Pointer pState);

    public static void rijndael128GCMProcessIV(Pointer pIV, int ivLen, Pointer pState)
            throws IPPCryptoException {

        int status = ippsRijndael128GCMProcessIV(pIV, ivLen, pState);
        if (status != IPPStatus.NO_ERR) {
            throw new IPPCryptoException(status);
        }
    }

    public static native int ippsRijndael128GCMProcessAAD(Pointer pAAD, int aadLen, Pointer pState);

    public static void rijndael128GCMProcessAAD(Pointer pAAD, int aadLen, Pointer pState)
            throws IPPCryptoException {

        int status = ippsRijndael128GCMProcessAAD(pAAD, aadLen, pState);
        if (status != IPPStatus.NO_ERR) {
            throw new IPPCryptoException(status);
        }
    }

    public static native int ippsHMACSHA256MessageDigest(Pointer pMsg, int msgLen, Pointer pKey,
            int keyLen, Pointer pMAC, int macLen);

    public static void hmacSHA256MessageDigest(Pointer pMsg, int msgLen, Pointer pKey, int keyLen,
            Pointer pMAC, int macLen) throws IPPCryptoException {

        int status = ippsHMACSHA256MessageDigest(pMsg, msgLen, pKey, keyLen, pMAC, macLen);
        if (status != IPPStatus.NO_ERR) {
            throw new IPPCryptoException(status);
        }
    }
}
