/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.ipp4j.cp;

/**
 * JNA interface to the IppAESGCMbehaviour enum (see ippcpdefs.h).
 * 
 * @author Raphael Yu Ning
 */
public enum IPPAESGCMBehavior {

    /**
     * Minimum memory, that is, the minimum size of the context.
     */
    DEFAULT(0),

    /**
     * Maximum protection against timing attacks.
     */
    SAFE(1),

    /**
     * Maximum performance of the implementation.
     */
    TABLE2K(2);

    private final int value;

    IPPAESGCMBehavior(int value) {

        this.value = value;
    }

    public int getValue() {

        return value;
    }
}
