/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.ipp4j.core;

/**
 * A class that represents generic IPP exceptions.
 * 
 * @author Raphael Yu Ning
 */
public class IPPException extends Exception {

    private static final long serialVersionUID = 2358496408569106085L;

    public IPPException(String msg) {

        super(msg);
    }

    public IPPException(int statusCode) {

        super(IPPCore.ippGetStatusString(statusCode));
    }
}
