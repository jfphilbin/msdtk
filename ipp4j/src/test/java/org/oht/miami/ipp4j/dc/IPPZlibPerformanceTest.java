/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.ipp4j.dc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class IPPZlibPerformanceTest {

    public static void main(String[] args) throws IPPZlibException, IOException {

        final String DCM_FILE_FILTER = "*.[Dd][Cc][Mm]";

        if (args.length < 2) {
            System.err.println("Usage: IPPZlibPerformanceTest"
                    + " <input dir> <output dir> [<compression level>]");
            System.exit(1);
        }

        File inputDir = new File(args[0]);
        File outputDir = new File(args[1]);
        outputDir.mkdirs();
        int compressionLevel = args.length < 3 ? IPPZlibLevel.DEFAULT_COMPRESSION : Integer
                .parseInt(args[2]);

        // Make sure native libraries are loaded by running a simple zlib test
        new IPPZlibTest().testCompressString();

        Path inputPath = inputDir.toPath();
        try (DirectoryStream<Path> inputDirStream = Files.newDirectoryStream(inputPath,
                DCM_FILE_FILTER)) {
            for (Path uncompressedFilePath : inputDirStream) {
                File uncompressedFile = uncompressedFilePath.toFile();
                File decompressedFile = new File(outputDir, uncompressedFile.getName());
                compressDecompressFile(uncompressedFile, decompressedFile, compressionLevel);
            }
        }
    }

    private static void compressDecompressFile(File uncompressedFile, File decompressedFile,
            int compressionLevel) throws IPPZlibException, IOException {

        int uncompressedSize = (int) uncompressedFile.length();
        ByteBuffer uncompressed = ByteBuffer.allocateDirect(uncompressedSize);
        FileInputStream fileIn = new FileInputStream(uncompressedFile);
        FileChannel fileChannel = fileIn.getChannel();
        uncompressed.position(IPPZlib.HEADER_LEN);
        int bytesRead = 0;
        try {
            do {
                bytesRead = fileChannel.read(uncompressed);
            } while (bytesRead != -1 && uncompressed.remaining() > 0);
        } finally {
            fileChannel.close();
        }
        uncompressed.flip();

        long compressionStart = System.nanoTime();
        ByteBuffer compressed = IPPZlib.compress(uncompressed, compressionLevel);
        long compressionEnd = System.nanoTime();
        int compressedSize = compressed.remaining();

        ByteBuffer decompressed = ByteBuffer.allocateDirect(uncompressedSize);
        long decompressionStart = System.nanoTime();
        IPPZlib.decompress(compressed, decompressed);
        long decompressionEnd = System.nanoTime();
        int decompressedSize = decompressed.remaining();
        assert decompressedSize == uncompressedSize;

        FileOutputStream fileOut = new FileOutputStream(decompressedFile);
        fileChannel = fileOut.getChannel();
        try {
            do {
                fileChannel.write(decompressed);
            } while (decompressed.remaining() > 0);
        } finally {
            fileChannel.close();
        }

        String fileName = uncompressedFile.getName();
        long compressionTime = compressionEnd - compressionStart;
        long decompressionTime = decompressionEnd - decompressionStart;
        System.out.println(fileName + "\t" + uncompressedSize + "\t" + compressedSize + "\t"
                + compressionTime + "\t" + decompressionTime);
    }
}
