/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.ipp4j.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.sun.jna.Pointer;

public class IPPCoreTest {

    @Test
    public void testIPPGetLibVersion() {

        IPPLibraryVersion libVersion = IPPCore.ippGetLibVersion();
        assertTrue(libVersion.name.length() > 0);
        assertTrue(libVersion.version.length() > 0);
        assertTrue(libVersion.buildDate.length() > 0);
        assertTrue(libVersion.getTargetCPU().length() > 0);
        assertEquals(7, libVersion.major);
    }

    @Test
    public void testIPPGetStatusString() {

        String statusString = IPPCore.ippGetStatusString(IPPStatus.LENGTH_ERR);
        assertEquals("ippStsLengthErr: Incorrect value for string length.", statusString);
    }

    @Test
    public void testIPPMalloc() throws IPPException {

        Pointer pointer = IPPCore.malloc(1);
        long address = Pointer.nativeValue(pointer);
        assertTrue(address % 64 == 0);
        pointer.setByte(0, (byte) 0xAB);
        IPPCore.ippFree(pointer);
    }

    @Test
    public void testIPPGetNumThreads() throws IPPException {

        int numThreads = IPPCore.getNumberOfThreads();
        assertTrue(numThreads > 0);
    }

    @Test
    public void testIPPGetNumCoresOnDie() {

        int numCores = IPPCore.ippGetNumCoresOnDie();
        assertTrue(numCores > 0);
    }
}
