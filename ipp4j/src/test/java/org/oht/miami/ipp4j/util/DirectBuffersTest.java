/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.ipp4j.util;

import static org.junit.Assert.assertTrue;

import org.oht.miami.ipp4j.util.DirectBuffers.Alignment;

import org.junit.Test;

import com.sun.jna.Memory;

public class DirectBuffersTest {

    @Test
    public void testAllocateAligned() {

        Memory memoryBlock = DirectBuffers.allocateAligned(1, Alignment._64);
        long address = Memory.nativeValue(memoryBlock);
        assertTrue(address % 64 == 0);
        assertTrue(memoryBlock.size() > 0);
    }
}
